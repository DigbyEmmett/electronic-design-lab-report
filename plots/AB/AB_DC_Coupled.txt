ANALOG
Ch 1 Scale 2.00V/, Pos 3.35000V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 10.000000 : 1, Skew 0.0s
Ch 2 Scale 2.00V/, Pos 3.37500V, Coup DC, BW Limit Off, Inv Off, Imp 1M Ohm
     Probe 10.000000 : 1, Skew 0.0s

TRIGGER
Sweep Mode Auto, Coup DC, Noise Rej Off, HF Rej Off, Holdoff 40.0ns
Mode Edge, Source Ch 1, Slope Rising, Level 912.50mV

HORIZONTAL
Mode Normal, Ref Center, Main Scale 410.0us/, Main Delay 0.0s

ACQUISITION
Mode Normal, Realtime On, Vectors On, Persistence Off

MEASUREMENTS
Pk-Pk(1), Cur 5.0V
Pk-Pk(2), Cur 4.7V

