\documentclass[11pt]{article}
\usepackage{hyperref}
\usepackage{multicol}
\usepackage{multirow}
\usepackage{float}
\usepackage{pgfplots}
\usepackage{amsmath}
\usepackage{circuitikz}[american]
\usetikzlibrary{calc}
\ctikzset{bipoles/resistor/height=.2}
\ctikzset{bipoles/resistor/width=0.5}
\usepackage{caption}
\usepackage{graphicx}
\graphicspath{{./plots/simulations/}}
\def\samples{200}
\usepackage{subcaption}
\usepackage[margin=1in]{geometry}
\title{Experiment: Type B, AB circuits as Power Amplifiers}
\author{Digby Emmett, Janus Silvestre}
\begin{document}
\maketitle
\section{Introduction}
Power amplifiers are often used in small circuits to amplify the power of an
input signal to some output source. This is often done for audio amplifiers
such as loud speakers. Power amplifiers gain this ability is in the use of both
PNP and NPN transistors which allow the amplification of both positive and
negative voltages where even necessary.

The NPN transistor is the easiest to understand. The diode is made up of three
separate silicon blocks each doped differently. For the NPN transistor the
Collector is doped N-type the base P-type and the emitter N-type. This is
shown in \autoref{fig:NPN} and \autoref{fig:PNP}.
\begin{figure}[!ht]
  \centering
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \begin{circuitikz}
      \draw[]
        node[npn]{};
    \end{circuitikz}
    \caption{An NPN Diode}
    \label{fig:NPN}
  \end{subfigure}
  \begin{subfigure}[t]{0.3\textwidth}
    \centering
    \begin{circuitikz}
      \draw[]
        node[pnp]{};
    \end{circuitikz}
    \caption{A PNP Diode}
    \label{fig:PNP}
  \end{subfigure}
  \caption{Transistor Types}
\end{figure}
Performing the appropriate calculations on these circuits gives the
following equations. Assuming voltages allow for forward active biasing. That
is the base is approximately 0.65 V above the emitter and the collector is
ideally 0.2V above the base leaving it around 0.85 V above the emitter to
ensure correct current flow.
\begin{equation}
   I_E = I_C + I_B
\end{equation}

Using these equations circuits can be easily solved for. This junction is the
easiest to understand as the current delivered through the collector can be
approximated by the equation $ I_C = \beta I_B $ while the same can be said for
the PNP transistor the current directions are slightly different due to
different doping.
\section{Background}
\subsection{Class B}
For the class B experiment as bi-product of using transistors there is a .65 V
drop from the input voltage to the output voltage on the positive half cycle
and a .65 drop from the output voltage on the negative half cycle. There is a
period during which the input signal produces no output for the range of output
values $V_{BE}>V_{in}>V_{EB}$ for this range the values of voltage are cut off
and no output voltage is observed. While this is very efficient in terms of
minimising power usage it is not ideal for practical applications. This is due
to the fact that the transistor needs to be turned on before it starts to
operate in active mode.
\begin{figure}[H]
\centering
\begin{circuitikz}[american]
  \draw[]
    node[pnp](a){}
    ++(0,1.5) node[npn](b){}
    (a.E) -- (b.E) -- ++(1,0) to[R,l=$R_L$] ++(0,-1) node[rground]{}
    (b.C) node[vcc]{$V_{CC}$}
    (a.B) -- (b.B)
    ($ (a.B) !.5! (b.B) $) -- ++(-1,0) node[sground]{} ++ (0,-1) node[]{$V_{in}$}
    (a.C) node[vee]{$-V_{CC}$}
  ;
\end{circuitikz}
\caption{Class B Power Amplifier Circuit}
\end{figure}

Given the circuit above the output voltage for the type B amplifier can be
solved for as shown below in \autoref{eqn:outputVoltageClassB}. These equations
define a piecewise function which can be plotted.
\begin{equation}\label{eqn:outputVoltageClassB}
  \begin{gathered}
    V_{out} = V_{in} - V_{BE_{NPN}}\\
    V_{out} = V_{in} + V_{EB_{PNP}}\\
  \end{gathered}
\end{equation}
The statement above is also true for the negative half cycle. The combination
of these signals forms the equation shown below in
\autoref{eqn:piecewiseClassBTheoertical}. As well as the
graphs shown in \autoref{fig:classBVoltagewaveform}
\begin{equation} \label{eqn:piecewiseClassBTheoertical}
  \begin{gathered}
    V_{out} =
       \begin{cases}
          V_{in} - V_{BE_{NPN}} & V_{in} > V_{BE_{NPN}} \\
          V_{in} + V_{EB_{PNP}} & V_{in} < V_{EB_{PNP}} \\
                  0             & elseware              \\
       \end{cases}
  \end{gathered}
\end{equation}
\begin{figure}[H]
\centering
\def\xs{0}
\def\xe{2*pi}
\begin{tikzpicture} [
    declare function = {
      input = 2.5*sin(deg(x));p1 = input - 0.65;p2 = input + 0.65;
      p3 = (p1>0);p4 = (p2<0);p5 =  p3*p1;p6 =  p4*p2;
      p7 = (p3+p4==0)*0;p8 =  p7+p6+p5;
      }]
    \begin{axis}
        [
        width=\textwidth,
        height=0.3\textheight,
        xmin=-0.1+\xs,
        xmax=\xe+0.1,
        ymin=-2.5,
        ymax=2.5,
        range=-2.5:2.5,
        samples=\samples,
        domain=\xs-0.1:\xe+0.1
        ]
    \addplot[red]{p8};
    \addplot[blue]{input};
    \legend{$V_{out}$,$V_{in}$}
    \end{axis}
\end{tikzpicture}
\caption{Expected waveform of $V_{out}$ and $V_{i}$ generated by \autoref{eqn:piecewiseClassBTheoertical}}
\label{fig:classBVoltagewaveform}
\end{figure}

Finally the power equations for the class B Amplifier can be expressed as shown
below in \autoref{eqn:classBPowerEfficiency}. Approximations are made to
simplify this procedure while results from the experiment will reflect more
accurately the actual results later on.
\begin{equation}\label{eqn:classBPowerEfficiency}
  \begin{gathered}
    P_{out} = \frac{1}{2}\frac{V^2_{out}}{R_L}\\
    P_{S+} =P_{S-} = \frac{1}{T}\int^{T}_{0}V_{in}I_{in} =
    \frac{1}{\pi}\frac{V_{out}}{R_L}V_{CC}\\
    P_{in} = (P_{S+}) + (P_{S-}) =
    2P_{S+}=\frac{2}{\pi}\frac{V_{out}}{R_L}V_{CC}\\
    \eta = \frac{P_{out}}{P_{in}} =
    \frac{1}{2}\frac{V^2_{out}}{R_L}\frac{\pi}{2}\frac{R_L}{V_{CC}V_{out}} =
    \frac{\pi}{4}\frac{V_{out}}{V_{CC}}
  \end{gathered}
\end{equation}

\iffalse
\begin{figure}[H]
\centering
\def\xs{0}
\def\xe{2*pi}
\begin{tikzpicture} [
    declare function = {
      vin = 2.5*sin(deg(x));p1 = vin - 0.65;p2 = vin + 0.65;
      p3 = (p1>0);p4 = (p2<0);p5 =  p3*p1;p6 =  p4*p2;
      p7 = (p3+p4==0)*0;vout =  p7+p6+p5;
      pout = 1/2*vout^2/100;
      p9 = 2/pi*vout/100*5;
      pin = abs(p9);
      eff = pout/pin;
      }]
    \begin{axis}
      [
      width=\textwidth,
      height=0.3\textheight,
      xmin=-0.1+\xs,
      xmax=\xe+0.1,
      ymin=-2.5,
      ymax=2.5,
      range=-2.5:2.5,
      samples=\samples,
      domain=\xs-0.1:\xe+0.1
      ]
    %\addplot[red]{pin};
    %\addplot[blue]{pout};
    \addplot[purple]{eff};
    \legend{$Efficiency$};
    \end{axis}
\end{tikzpicture}
\caption{Expected waveform of $V_{out}$ and $V_{i}$ generated by \autoref{eqn:piecewiseClassBTheoertical}}
\label{fig:classBVoltagewaveform}
\end{figure}
\fi
\subsection{Class AB}
For class AB type power amplifier there is no need for the input signal to
force active mode on either output transistor. This is because class AB is
designed to sit in alive mode while idle. The use of an extra pair of transistors to
provide a small voltage source to the load. This is easily done using two
transistors to provide a bias current. For this to work it is essential to
ensure that the bias current supplied is large enough to not be affected by the
current demand required by the amplifier when the signal swings high or low.
The solution for the current through the load is shown in the appendix under
\autoref{eqn:ABCurrentDerivation}

Shown below in \autoref{fig:classABCircuit} is the circuit used to construct an AB amplifier in order to conduct
the experiment. This circuit is designed as per
above using two PNP and two NPN transistors biased with a supply voltage of +5V
feeding the bias side through a resistor which will control the current
supplied.

\begin{figure}[H]\label{fig:classABCircuit}
   \centering
   \begin{circuitikz}
      \draw
      node[pnp,xscale=-1](a){} ++(0,2) node[npn,xscale=-1](b){} ++(2,0)
      node[npn](c){} ++(0,-2) node[pnp](d){}
      (a.E) -- ($ (a.E) !.5! (b.E) $) node[circ]{} -- (b.E)
      (b.C) to[R,l=$R_{bias}$] ++(0,1) node[vcc]{$V_{CC}$}
      (b.C) -- (b.C)-|(b.B) -- (b.B) -- (c.B)

      (a.C) -- (a.C) -| (a.B) -- (a.B) -- (d.B)
      (c.E) -- ($ (c.E) !.5! (d.E) $) node[circ](e){} -- ++(1,0) to[R,l=$R_L$]
      ++(0,-1) node[rground]{}
      (e) -- (d.E)
      (c.C) node[vcc]{$V_{CC}$}
      (a.C) node[sground]{} ++(-1,0) node{$V_{in}$}
      (d.C) node[vee]{$-V_{CC}$}
      ;
   \end{circuitikz}
   \caption{Class AB Amplifier}
   \label{fig:classABCircuit}
\end{figure}

In order to design a functioning circuit it is essential to start from the
output. In this case similar to above this is a resistor with a maximum power
output of 0.25W. The theoretical maximum voltage which could be delivered to
the resistor is given when there is no voltage drop across the NPN transistor.
In this case the voltage at the top of the minimum resistance value is
calculated below.
\begin{equation}
   \begin{gathered}
      P_{load} = \frac{V^2_{cc}}{R_L}\\
      R_{L_{max}}=\frac{V^2_{cc}}{P_{out}}=100\Omega\\
   \end{gathered}
\end{equation}
This resistance will be used as the maximum voltage will rarely be reached and
this will allow for the largest power output on smaller $V_{out}$ signals.

The maximum current provided to the load in this case is shown below without
consideration of the transistor from which it is sourced.
\autoref{}
\begin{equation}
  \begin{gathered}
    I_{max} = \frac{P_{max}}{V_{cc}} = \frac{0.25mW}{5V}=50mA
  \end{gathered}
\end{equation}
The transistors maximum power can be found in the data sheet and is listed as
500mW. The power being delivered is well below the maximum rating for the
transistors.

Keeping the derivations above in mind and moving forward with the analysis the
maximum current which will be provided through the bias of the NPN transistor
can be calculated for the maximum power delivery $I_{C_{max}}=50mA$. This
derivation is shown below for $\beta=250$
\begin{equation}
  \begin{gathered}
    I_C = \beta I_B\\
    I_B = \frac{I_C}{\beta} = 0.2mA\\
  \end{gathered}
\end{equation}
In order to always satisfy this condition the bias current $I_{bias}$ should be
far greater than the base current as to ensure no variation in active behaviour
occurs. For this derivation $I_{bias}\approx 1mA$ to to decide the final value
of $I_{bias}$ KVL will be performed using standard value resistors selected
from those available. The derivation of the bias resistor is shown below.
\begin{equation}
  \begin{gathered}
    V_{cc} - I_{bias}\cdot R_{bias} + V_{BE_{bias}} +V_{EB_{bias}}=0\\
    R_{bias} = \frac{V_{cc}-\left(V_{BE_{bias}}
    +V_{EB_{bias}}\right)}{I_{bias}}\\
    R_{bias} = \frac{5V - 0.65V - 0.65V}{0.8mA} = 4.625k\Omega \approx
    4.7k\Omega\\
  \end{gathered}
\end{equation}
A resistance value of 4.7k will be used. This will produce a current of
$I_{bias} = .78mA$ at the input to the circuit. This bias current will force
the transistors on the left to remain active and allow for the bias circuit to
easily switch between the NPN and PNP junction transistors to provide a full
sinusoidal output wave form. The waveform which is produced should only be
limited by the saturation current and voltages of the supply transistors if
the biasing has been performed correctly. The new circuit is shown below
assuming a constant current source and two diode drops across which the bias
current flows.
\begin{figure}[H]
   \centering
   \begin{circuitikz}
      \draw
      (0,0) to[Do,invert] (0,1.5) to[Do,invert] (0,3)
      (2,3) node[npn](c){} ++(0,-3) node[pnp](d){}
      (0,3) to[american current source,invert] ++(0,1.5) node[vcc]{}
      (0,3) -- (c.B)

      (0,0) -- (d.B)
      (c.E) to[short,i_=$I_N$] ($ (c.E) !.5! (d.E) $) node[circ](e){}
      to[short,i_=$I_L$]  ++(1,0) to[R,l=$R_L$]
      ++(0,-1) node[rground]{}
      (e) to[short,i_=$I_P$]  (d.E)

      (c.C) node[vcc]{$V_{CC}$}
      (0,0) node[sground]{} ++(0,-1) node{$V_{in}$}
      (d.C) node[vee]{$-V_{CC}$}
      ;
   \end{circuitikz}
   \caption{Class AB Amplifier Equivalent Circuit}
   \label{fig:classABCircuit}
\end{figure}
For each diode there is a .65V drop. These can be used to form an expression
for the voltage observed at the output of the amplifier The fundamental
equation is shown below. Full working is shown in the appendices
(\autoref{eqn:ABcurrent}).
\begin{equation}
      i_{N} + i_L + i_{P} = 0\\
\end{equation}
\begin{equation}
      2V_{D} + V_{BEN} + V_{EBN} = 0\\
\end{equation}
\begin{equation}
  \begin{gathered}
    i=I_S e^{\frac{v_{BE}}{V_T}}\\
  \end{gathered}
\end{equation}
The result of the combination of these equations is the system shown
below. This system described the voltages and currents with respect to each
other during operation of the AB amplifier. A more informative form of this
equation is present in the derivation which taked place in the
appendices under equation \label{eqn:ABcurrent}. This derivation implies that
with an increase in $I_N$ current an decrease in $I_P$ will occur. This is
important to understand as both transistors are not alwasy active and infact
there is only a small period of time where both conduct equal amounts.
\begin{equation}
   \begin{gathered}
    I^2_N = I_L\cdot I_N + I^2_Q
   \end{gathered}
\end{equation}
$I_Q$ is the quiescent current which flows through the NPN emitter and PNP
collector. The power present at the output terminal of the AB circuit is
equivalent to the power which is output for the class B circuit. The input
power is equivalent to the class B circuit with the addition of the biasing
circuit power which is always active. This derivation is shown below.
\begin{equation}
  \begin{gathered}
  P_{in} = P_{classB_{in}} + P_{bias}\\
  P_{bias} = V_{S}\cdot \frac{V_{S} - V_{BE(on)} - V_{EB(on)}}{R_{bias}}
  \end{gathered}
\end{equation}
The efficiency can then be shown using the output power derived above
\begin{equation}
  \begin{gathered}
    \eta = \frac{P_{out}}{P_{in}} =
    \frac{\frac{V^2_{out}}{2
    R_L}}{\frac{2}{\pi}\frac{V_{out}}{R_L}V_{CC}+V_{CC}\cdot \frac{V_{CC} - V_{BE(on)} - V_{EB(on)}}{R_{bias}} }
  \end{gathered}
\end{equation}
\section{Experiment}
\subsection{Initial Measurements}
Initially measurements be taken for the respective
\beta values of the transistors. These measurements have been
tabulated below.
\begin{table}[H]
  \centering
  \begin{tabular}{| l | l | c | c |}
    \hline
     & \textbf{Transistor} & \textbf{Left} & \textbf{Right}  \\
    \hline
    \multirow{2}{1em}{\beta} & BC548 & 245 & 244 \\
                             & BC558 & 420 & 425 \\
    \hline
  \end{tabular}
    \caption{Results from preliminary transistor measurements}
\end{table}

These results reflect very well matched transistors.
\subsection{Class B}
In order to analyse the class B circuit the currents will need to be measured
along with the voltage found at the output and input signals. This will allow
for the appropriate calculations to be performed later.
\begin{figure}[H]
  \centering
  \includegraphics[width=0.6\textwidth]{ClassB5V.png}
  \caption{Class B simulation results for 5V 1kHz input signal}
\end{figure}
The currents measured through transistors in the class B measurement were as
follows.
\begin{table}[H]
\centering
  \begin{tabular}{|l|r|}
    \hline
    Measurement & Value\\
    \hline
    $I_{S+}$ & $17.5mA$\\
    $I_{S-}$ & $2.6mA$ \\
    $V_{out}$& $3.8V$ \\
    \hline
  \end{tabular}
  \caption{Current Results from Class B measurements}
\end{table}
\begin{equation}\label{eqn:classbefficiency}
  \begin{gathered}
P_{in} = \frac{2V_{out}}{\pi R_L}\cdot V_{CC} =
\frac{2\cdot 1.9}{\pi\cdot 100}\cdot 5 = 60.47mW\\
  P_{out} = \frac{V^2_{out}}{2 R_L} =
  \frac{1.9^2}{2\cdot100} = 18.05mW\\
  \eta = \frac{P_{out}}{P_{in}} = \frac{18.05}{60.47} = 29.95\%
  \end{gathered}
\end{equation}
Using the efficiency calculations defined above in the background the
efficiency was found to be 29.95\%. This efficiency is reasonably poor as
compared with the theoretical maximum efficiency.
\subsection{Class AB}
For the class AB experiment the circuit shown below was used. In order to
calculate power efficiency the currents and voltages are required. These
currents were measured and have been tabulated below.
\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
  \includegraphics[width=\textwidth]{ClassAB5VDC.png}
  \caption{DC Coupled Response}
    \label{fig:NPN}
  \end{subfigure}
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
  \includegraphics[width=\textwidth]{ClassAB5VAC.png}
  \caption{AC Coupled Response}
    \label{fig:PNP}
  \end{subfigure}
  \caption{Class AB response to 5V 1kHz input.}
\end{figure}
The output voltage waveform is shown below for the Class AB response. This
signal performed as expected with the output of the signal starting .325V above
the input signal and a small amount of distortion occurring at the base of the
transistor.
\begin{table}[H]
\centering
  \begin{tabular}{|l|c|c|c|c|}
   \hline 
                 & $I_{bias}$ & $I_{S+} $ & $ I_{S-} $ & $V_{out} $ \\
  \hline 
     DC Coupled  &  750\mu A  & 11.4mA    & 3.996mA & 4.7V   \\
     AC Coupled  &  733\mu A  &  7.12mA   & 7.57mA & 4.6V    \\
   \hline 
  \end{tabular}
  \caption{Class AB AC and DC coupled measurements}
\end{table}
\begin{equation}
  \begin{gathered}
    P_{in} = 5 \cdot \left( 7.57 + 7.12 \right) + .733 \cdot 5 = 77.115mW\\
    P_{out} = \frac{2.3^2}{2\cdot 100} = 26.45mW\\
    \eta = \frac{P_{out}}{P_{in}} = \frac{77.115}{26.45} = 34\%
  \end{gathered}
\end{equation}

\section{Discussion}

The efficiency of the class B amplifier was observed to be approximately 30\%
This was far less than the theoretical maximum efficiency of the class b
amplifier which is obtained when the output voltage is equal to the input
voltage as shown in \autoref{eqn:classbefficiency}.

The class AB amplifier provided a power output efficiency of a small amount
less than the efficiency of the class B amplifier due to the addition of the
biasing supply voltage on the input power. The results the experiment returned
showed an efficiency of 34\% for The class AB amplifier. This was unexpected as
the theoretical maximum was less than that of the class B amplifier and the
circuit was built off the class B amplifier. In theory the class B amplifier
should have had the highest efficiency given it was tested under the same
conditions. This discrepancy could have been caused by a measurement error
during the experiment either while measuring the currents of the class B or the
class AB amplifier. Either way the measurements were below the theoretical
maximum and thus can be considered plausible.

In order to further improve the efficiency of the class B amplifier it was
theorised that the addition of negative feedback would assist in the correction
of distortion in the output waveform compared to the class B amplifier. For our
experiment, the effects of adding an operational amplifier to the Class B
circuit to provided negative feedback was observed.  The positive terminal
was connected to the input signal, the negative terminal was connected to the
output of the Class B stage and the output of the op amp was used to drive the
input of the original Class B circuit from before. This is shown below:
\begin{figure}[H]
  \centering
  \begin{circuitikz}[american,scale=0.7]
      \draw
      node[pnp](a){} ++(0,2) node[npn](b){} ++(-3,-1) node[op amp](c){}
      (c.out) |- (a.B) -- (a.B)
      (c.out) |- (b.B)
      (a.E) -- (b.E) -- node[circ](d){}++(1,0) to[R,l=$R_L$] ++(0,-1) node[rground]{}
      (b.C) node[vcc]{} ++(-1,0)node{$V_{CC}$}
      (a.C) node[vee]{} ++(-1,0) node{$-V_{CC}$}
      (d) -- ++(0,3) -- ++(-5.2,0) -- (c.-)
      (c.+) -- ++(-1,0) to[V,l=$V_{in}$] ++(0,-1.5) node[rground]{}
      ;
  \end{circuitikz}
  \caption{Modified Class B circuit including additional Op Amp for negative
  feedback}
\end{figure}

The original experiment was conducted, a 5 V peak to peak sinusoidal voltage at
1kHz was used as the input. The output voltage can be seen as below:

\begin{figure}[H]
\centering
\includegraphics[width=0.6\textwidth]{ClassB5V1kOpAmp.png}
\caption{Class B with feedback response to 5V 1kHz}
\end{figure}
Two key differences can be seen when comparing this to the original class B
circuit. The crossover distortion has been mostly eliminated and the peak
voltages are hitting the maximum without the voltage drop due to the $V_{BE}$ on and
$V_{EB}$ on. The added op amp tries to keep the input to its two terminals at the
same voltage (due to the input differential amplifier stage) and can thus
“sense” when the circuit approaches the dead band zone. It can then adjust its
output current (the input to Class B) such that the output of Class B is the
same as the input signal.

However, there are still several issues that this configuration faces. The op amp has
a limited slew rate and as such, can only maintain the linearity of the output
below a certain frequency. This reintroduces distortion to the output
signal that gets worse with increasing frequency. The output waveforms of a
5kHz and a 10kHz sine wave input below demonstrates how limited the effective
bandwidth of this circuit is as there is significant distortion even at
relatively low frequencies.
\begin{figure}[H]
  \centering
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
  \includegraphics[width=\textwidth]{ClassB5V5kOpAmp.png}
  \caption{Response to 5Vpp 5kHz input}
    \label{fig:NPN}
  \end{subfigure}
  \begin{subfigure}[t]{0.45\textwidth}
    \centering
  \includegraphics[width=\textwidth]{ClassB5V10kOpAmp.png}
  \caption{Response to 5Vpp 10kHz input}
    \label{fig:PNP}
  \end{subfigure}
  \caption{Responses}
\end{figure}

Another problem with this configuration is the added power losses in the
system. Introducing the op amp also most likley results in a reduction in
overall efficiency due the power dissipated in the op amp.

In comparison to a Class AB amplifier, this amplifier seems to be overall a
worse solution. Even though it corrects its cross over distortion. This is due
to its narrow effective bandwidth that it operates at without significant
distortion.
\section{Conclusion}
It was found that the theoretical maximum efficiency for amplification using
transistors for power amplification was found to be $\frac{\pi}{4}$ but the
experimental results produced an efficiency of at most 34\%. The addition of an
op amp to provide negative feedback theoretically could improve this however
this seems unlikely as the power supplied to the op amp would most likely be
greater than the power gained from the correction in the sinusoidal output of
the amplifier.

The lab equipment was returned on 29/10/2019. Nicks signature is shown below
confirming the return of the equipment.
\begin{figure}[H]
   \centering
   \includegraphics[width=0.8\textwidth]{Nick.jpg}
\end{figure}
\section{Appendix}
\begin{equation}\label{eqn:ABcurrent}
   \begin{gathered}
      i_{Q} = I_S e^\frac{V_{BB}}{2i\cdot V_{T}}\\
      i_{N} = I_S e^\frac{V_{BEN}}{V_{T}}\\
      i_{P} = I_Q e^\frac{V_{EBP}}{V_{T}}\\
      V_{BB} = 2 V_T \cdot \ln\left(\frac{i_Q}{I_S}\right)\\
      V_{BEN} = V_T \cdot\ln\left(\frac{i_N}{I_S}\right)\\
      V_{EBP} = V_T \cdot \ln\left(\frac{i_P}{I_S}\right)\\
      2 V_T \cdot \ln\left(\frac{i_Q}{I_S}\right) = V_T
      \cdot\ln\left(\frac{I_N}{I_S}\right) + V_T \cdot
      \ln\left(\frac{I_P}{I_S}\right) \to
      \ln\left[\left(\frac{I_Q}{I_S}\right)^2\right] = 
      \cdot\ln\left(\frac{I_N}{I_S}\right) + \cdot \ln\left(\frac{I_P}{I_S}\right)\\
       \frac{I_Q^2}{I_S^2} = 
      e^{\ln\left(\frac{i_N}{I_S}\right) + \ln \left(\frac{I_P}{I_S}\right)}
      \to
      \frac{I_Q^2}{I_S^2} = 
      \frac{I_N}{I_S}\frac{I_P}{I_S} \to I^2_Q = I_N\cdot I_P \to I_P =
      \frac{I^2_Q}{I_N}\\
      I_N = I_L + \frac{I^2_Q}{I_N}\to I^2_N = I_L\cdot I_N + I^2_Q
   \end{gathered}               
\end{equation}
\end{document}
